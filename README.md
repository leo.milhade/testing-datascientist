CONSIGNES TEST TECHNIQUE
========================

Utilisation du langage de votre choix (préférentiellement python 3.X)
---------------------------------------------------------------------

2h pour réaliser ces tâches :

* Import de la donnée shom_data.csv comportant 4 colonnes de descripteur et 1 colonne de label (Whale, Dolphin ou Shark).
* Visualiser le jeu de données selon les dimensions qui vous semblent les plus pertinentes.
* Filtrer le jeu de données si nécessaire.
* Décrire succinctement ce dernier (stat min/max, longueur, types…).
* Réaliser une clusterisation non-supervisée avec la méthode de votre choix (expliquer ce choix).
* Réaliser une clusterisation supervisée avec la méthode de votre choix (expliquer ce choix).
* Visualiser le résultat de ces classifications.
* Réaliser un modèle de régression avec apprentissage permettant la prédiction de nouvelles valeurs.
* Evaluer et visualiser le modèle.
* Conclure sur les méthodes utilisées.

Merci d’envoyer vos résultats à l’adresse suivante : julian.le.deunf@shom.fr

Si vous avez des questions vous pouvez également joindre cette adresse.

Bon courage !

